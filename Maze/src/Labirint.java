import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.util.Scanner;
import java.util.Vector;
import javax.swing.JPanel;

public class Labirint  extends JPanel{

	enum Culori {
        Alb,
        Albastru,
        Negru,
        Verde,
        Rosu,
        Necolorat
    }
	private Vector<Vector<Integer>> matrice = new Vector<Vector<Integer>>();
    private Vector<Vector<Integer>> matriceNoduri = new Vector<Vector<Integer>>();
    private Vector<Vector<Integer>> listeAdiacenta = new Vector<Vector<Integer>>();
    private Vector<Integer> iesiri = new Vector<Integer>();
    private Vector<Vector<Culori>> noduriColorate = new Vector<Vector<Culori>>();
    private int numarNod = 0;
    private int nodStart;
   // private int nodEnd;
    private boolean apasat = false;
    private Vector<Integer> p = new Vector<Integer>();
	 public void citireMatrice() 
	 {
	        File fisier = new File("Labirint.txt");
	        try {
	            Scanner citire = new Scanner(fisier);
	            int n = citire.nextInt();
	            int m = citire.nextInt();	            
	            for (int i = 0; i < n; i++)
	            {
	                Vector<Integer> linie = new Vector<Integer>();
	                for (int nod = 0; nod < m; nod++) 
	                {
	                    int numarNod = citire.nextInt();
	                    linie.add(numarNod);
	                }
	                matrice.add(linie);
	            }
	            citire.close();
	        } catch (Exception exceptie) {
	            System.out.println("Eroare!");
	        }
	    }
	 public void afisareListeAdiacenta(Vector<Vector<Integer>> matrice)
	 {
		 for(int i=0;i<matrice.size();i++)
		 {
			 System.out.print((i+1)+": ");
			 for(int j=0;j<matrice.get(i).size();j++)
				 System.out.print(matrice.get(i).get(j)+" ");
			 System.out.println();
		 }
	 }
	 public void afisareMatrice(Vector<Vector<Integer>> matrice)
	 {
		 for(int i=0;i<matrice.size();i++)
		 {
			 for(int j=0;j<matrice.get(i).size();j++)
				 System.out.print(matrice.get(i).get(j)+" ");
			 System.out.println();
		 }
	 }
	    public void creareMatriceNoduri() 
	    {
	        for (int i = 0; i < matrice.size(); i++) 
	        {
	            Vector<Integer> vectorLinie = new Vector<Integer>();
	            Vector<Culori> vectorCulori = new Vector<Culori>();
	            for (int j = 0; j < matrice.get(i).size(); j++) 
	            {
	                if (matrice.get(i).get(j) != 0) //daca nu e zid
	                {
	                    numarNod++;//adaugam in vectorul de noduri
	                    vectorLinie.add(numarNod);
	                    if (matrice.get(i).get(j) == 2) 
	                    {
	                        nodStart = numarNod;//punct de intrare
	                    }
	                    //inseamna ca e 3,adica iesire din labirint
	                    if (matrice.get(i).get(j) == 3) 
	                    {
	                        iesiri.add(numarNod);	                 
	                    }
	                } 
	                else//este zid, adica 0
	                {
	                    vectorLinie.add(0);
	                }
	                vectorCulori.add(Culori.Necolorat);
	            }
	            noduriColorate.add(vectorCulori);
	            matriceNoduri.add(vectorLinie);
	        }
	    }
	    public void graf()
	    {	//alocare de memorie
	       for (int i = 0; i < numarNod; i++) 
	        {
	            Vector<Integer> vectorAuxiliar = new Vector<Integer>();
	            listeAdiacenta.add(vectorAuxiliar);
	        }
	   
	        for (int i = 0; i < matriceNoduri.size(); i++)
	        {
	            for (int j = 0; j < matriceNoduri.get(i).size(); j++)
	            {
	            	//get- ce se afla pe pozitia respectiva
	            	//add -adauga ceva pe pozitia respectiva
	                if (matriceNoduri.get(i).get(j) != 0)
	                {
	                    if (i > 0 && matriceNoduri.get(i - 1).get(j) != 0) 
	                    {
	                        listeAdiacenta.get(matriceNoduri.get(i - 1).get(j) - 1).add(matriceNoduri.get(i).get(j));
	                    }
	                    if (i < matriceNoduri.size() - 1 && matriceNoduri.get(i + 1).get(j) != 0) 
	                    {
	                        listeAdiacenta.get(matriceNoduri.get(i + 1).get(j) - 1).add(matriceNoduri.get(i).get(j));
	                    }
	                    if (j > 0 && matriceNoduri.get(i).get(j - 1) != 0)
	                    {
	                        listeAdiacenta.get(matriceNoduri.get(i).get(j - 1) - 1).add(matriceNoduri.get(i).get(j));
	                    }
	                    if (j < matriceNoduri.get(i).size() - 1 && matriceNoduri.get(i).get(j + 1) != 0) {
	                        listeAdiacenta.get(matriceNoduri.get(i).get(j + 1) - 1).add(matriceNoduri.get(i).get(j));
	                    }
	                }
	            }
	        }
	       
	    }
	    private void drum(int iesire) {
	        while (iesire != nodStart) 
	        {
	            for (int i = 0; i < matriceNoduri.size(); i++)
	            {
	                for (int j = 0; j < matriceNoduri.get(i).size(); j++) 
	                {
	                    if (matriceNoduri.get(i).get(j) == iesire)
	                    {                    	
	                    	noduriColorate.get(i).set(j,Culori.Verde);
	                    	break;	                    
	                    }
	                }
	            }
	            iesire = p.get(iesire - 1);
	        }
	    }
	    private void parcurgereLatime()
	    {

	        int infinit = Integer.MAX_VALUE;
	        Vector<Boolean> U = new Vector<Boolean>();
	        Vector<Integer> V = new Vector<Integer>();
	        Vector<Integer> W = new Vector<Integer>();
	        Vector<Integer> o = new Vector<Integer>();
	        for (int i = 0; i < listeAdiacenta.size(); i++)
	        {
	            p.add(0);
	            o.add(infinit);
	            U.add(false);
	        }
	        U.set(nodStart - 1, true);
	        V.add(nodStart);
	        W = new Vector<Integer>();
	        o.set(nodStart - 1, 0);
	        while (V.size() != 0)
	        {
	            int x = V.firstElement();
	            for (int y : listeAdiacenta.get(x - 1)) 
	            {
	                if (U.get(y - 1) == false)
	                {
	                    U.set(y - 1, true);
	                    V.add(y);
	                    p.set(y - 1, x);
	                    o.set(y - 1, o.get(x - 1) + 1);
	                    if (iesiri.contains(y)) 
	                    {
	                        drum(y);
	                    }
	                }
	            }
	            V.remove(V.firstElement());
	            W.add(x);
	        }
	        repaint();
	    }
	    public void butonApasat()
	    {
	        apasat = true;
	        parcurgereLatime();
	    }
	    protected void paintComponent(Graphics grafica)
	    {
	        super.paintComponent(grafica);
	        if (apasat == false) 
	        {
	            for (int i = 0; i < matriceNoduri.size(); i++)
	            {
	                for (int j = 0; j < matriceNoduri.get(i).size(); j++)
	                {
	                    if (matriceNoduri.get(i).get(j) == 0) 
	                    {
	                        noduriColorate.get(i).set(j, Culori.Negru);	                        
	                        grafica.setColor(Color.BLACK);
	                        grafica.fillRect(35 * j + 70, 35 * i + 70, 35, 35);
	                        grafica.setColor(Color.BLACK);
	                        grafica.drawRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    } 
	                    else 
	                    	if (matriceNoduri.get(i).get(j) == nodStart)
	                    {
	                        noduriColorate.get(i).set(j, Culori.Albastru);
	                        grafica.setColor(Color.BLUE);
	                        grafica.fillRect(35 * j + 70, 35 * i + 70, 35, 35);
	                        grafica.setColor(Color.BLACK);
	                        grafica.drawRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    }
	                    	else 
	                    		if(matriceNoduri.get(i).get(j) != 0 && 
	                    					(i==0 || i==(matriceNoduri.size()-1)||j==0 
	                    					||j==(matriceNoduri.size()+2)))
	                    			{
	                    				noduriColorate.get(i).set(j, Culori.Rosu);
		                    			grafica.setColor(Color.RED);
		                    			grafica.fillRect(35 * j + 70, 35 * i + 70, 35, 35);
		                    			grafica.setColor(Color.BLACK);
		                    			grafica.drawRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    			}
	                    				
	                    	else 
	                    		if (matriceNoduri.get(i).get(j) != 0 &&
	                    					(i!=0 || i!=(matriceNoduri.size()-1)||j!=0 
	                    					||j!=(matriceNoduri.size()-1)))
	                    			{              
	                    				noduriColorate.get(i).set(j, Culori.Alb);
	                    				grafica.setColor(Color.WHITE);
	                    				grafica.fillRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    				grafica.setColor(Color.BLACK);
	                    				grafica.drawRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    			}	                    		
	                    }
	                    	
	                }
	            }
	        
	        else
	        {
	            for (int i = 0; i < matriceNoduri.size(); i++)
	            {
	                for (int j = 0; j < matriceNoduri.get(i).size(); j++) {
	                    switch (noduriColorate.get(i).get(j)) 
	                    {
	                        case Alb:
	                            grafica.setColor(Color.WHITE);
	                            break;
	                        case Negru:
	                            grafica.setColor(Color.BLACK);
	                            break;
	                        case Albastru:
	                            grafica.setColor(Color.BLUE);
	                            break;
	                        case Verde:
	                            grafica.setColor(Color.GREEN);
	                            break;
	                        default:
	                            break;
	                    }
	                    grafica.fillRect(35 * j + 70, 35 * i + 70, 35, 35);
	                    grafica.setColor(Color.BLACK);
	                    grafica.drawRect(35 * j + 70, 35 * i + 70, 35, 35);
	                }
	            }
	        }
	    }

	    public Labirint()
	    {
	    	citireMatrice();
	    	creareMatriceNoduri();
	    	graf();
	    	repaint();
	    	afisareMatrice(matriceNoduri);
	    	afisareListeAdiacenta(listeAdiacenta);
	    	
	    }
}
