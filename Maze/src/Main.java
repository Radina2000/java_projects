import java.awt.Button;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class Main {

	private static void initializareInterfata() 
	 {
		 Labirint labirint = new Labirint();
	        Button buton = new Button();
	        buton.setSize(100, 30);
	        buton.setLabel("Afisare drumuri");
	        buton.addActionListener( new ActionListener()
	        {

	            public void actionPerformed(ActionEvent ae) 
	            {
	                labirint.butonApasat();
	            }
	  });
	        JFrame frame = new JFrame("Algoritmica Grafurilor");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.add(buton);
	        frame.add(labirint);
	        frame.setSize(500, 400);
	        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	        frame.setLocation(dim.width/2-frame.getSize().width/2, dim.height/2-frame.getSize().height/2);
	        frame.setVisible(true);  
	 }
	public static void main (String[] args) 
	{
        SwingUtilities.invokeLater( new Runnable()
        {

            public void run() {

                initializareInterfata();
            }
        });
    }
}
