import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
public class Main {
    public static ArrayList<ZonaParcari<String,Integer>> listaZonaParcari=new ArrayList<ZonaParcari<String,Integer>>();
    public static ArrayList<ZonaComerciala<String,Integer>> listaZonaComerciala=new ArrayList<ZonaComerciala<String,Integer>>();
	public static void CitireParcari(String readFile) throws FileNotFoundException
	{
		File file=new File(readFile);
		Scanner sc=new Scanner(file);
		//ArrayList<ZonaParcari<String,Integer>> listaZonaParcari=new ArrayList<ZonaParcari<String,Integer>>();
		
			
			int nr=sc.nextInt();
			for(int i=0;i<nr;i++)
		{
				
			String denumireZona=sc.next();
			int nordZona=sc.nextInt();
			int sudZona=sc.nextInt();
			int vestZona=sc.nextInt();
			int estZona=sc.nextInt();
			int numar=sc.nextInt();
			ArrayList<Parcari<String,Integer>> listaParcari=new ArrayList<Parcari<String,Integer>>();
			for(int k=0;k<numar;k++)
			{
			String denumire=sc.next();
			Integer nord=sc.nextInt();
			Integer sud=sc.nextInt();
			Integer vest=sc.nextInt();
			Integer est=sc.nextInt();
			Integer nrLocuri=sc.nextInt();
			Integer nrLibere=sc.nextInt();
			Parcari<String,Integer> parc =new Parcari<String,Integer>(denumire,nrLocuri,nord,sud,est,vest,nrLibere);
			listaParcari.add(parc);
			}
			ZonaParcari<String,Integer> zone=new ZonaParcari<String,Integer>(denumireZona,listaParcari,nordZona,sudZona,estZona,vestZona);
			listaZonaParcari.add(zone);
			
			
		}
		
		
		sc.close();
		System.out.println(listaZonaParcari.size());
		for(int i=0;i<listaZonaParcari.size();i++)
		{
			System.out.println(listaZonaParcari.get(i));
		}
	}
	public static void CitireMagazine(String readFile) throws FileNotFoundException
	{
		File file=new File(readFile);
		Scanner sc=new Scanner(file);
		//ArrayList<ZonaComerciala<String,Integer>> listaZonaComerciala=new ArrayList<ZonaComerciala<String,Integer>>();
		
			
			int nr=sc.nextInt();
			for(int i=0;i<nr;i++)
		{
				
			String denumireZona=sc.next();
			int nordZona=sc.nextInt();
			int sudZona=sc.nextInt();
			int vestZona=sc.nextInt();
			int estZona=sc.nextInt();
			int numar=sc.nextInt();
			ArrayList<Magazine<String,Integer>> listaMagazine=new ArrayList<Magazine<String,Integer>>();
			for(int k=0;k<numar;k++)
			{
			String denumire=sc.next();
			Integer nord=sc.nextInt();
			Integer sud=sc.nextInt();
			Integer vest=sc.nextInt();
			Integer est=sc.nextInt();
			String categorie=sc.next();
			Magazine<String,Integer> magazine =new Magazine<String,Integer>(denumire,categorie,nord,sud,est,vest);
			listaMagazine.add(magazine);
			}
			ZonaComerciala<String,Integer> zone=new ZonaComerciala<String,Integer>(denumireZona,listaMagazine,nordZona,sudZona,estZona,vestZona);
			listaZonaComerciala.add(zone);
			
			
		}
		
		
		sc.close();
		System.out.println(listaZonaComerciala.size());
		for(int i=0;i<listaZonaComerciala.size();i++)
		{
			System.out.println(listaZonaComerciala.get(i));
		}
	}
	public static void Ex1(ArrayList<ZonaParcari<String,Integer>> listaZonaParcari,Integer n,Integer s,Integer e,Integer v)
	{ 
		Integer nord,sud,est,vest;
		int pozitieZona=Integer.MAX_VALUE,pozitieParcare=Integer.MAX_VALUE;
		Integer distanta=0;
		Integer distanta_min=Integer.MAX_VALUE;
		for(int i=0;i<listaZonaParcari.size();i++)
		{
			for(int j=0;j<listaZonaParcari.get(i).getParcari().size();j++)
			{
				if(listaZonaParcari.get(i).getParcari().get(j).getLocuri()!=0)
				{nord=listaZonaParcari.get(i).getParcari().get(j).gerCordNord();
				sud=listaZonaParcari.get(i).getParcari().get(j).gerCordSud();
				est=listaZonaParcari.get(i).getParcari().get(j).gerCordEst();
				vest=listaZonaParcari.get(i).getParcari().get(j).gerCordVest();
				distanta=Math.abs(((n+s)/2)-((nord+sud)/2)) + Math.abs(((e+v)/2)-((est+vest)/2));
				if(distanta_min>distanta)
				{
					distanta_min=distanta;
					pozitieZona=i;
					pozitieParcare=j;
				}}
			}
		}
		System.out.println("cea mai apropiata zona:"+listaZonaParcari.get(pozitieZona).getDenumire());
		System.out.println(listaZonaParcari.get(pozitieZona).getParcari().get(pozitieParcare));
		
	}
	public static void Ex4(ArrayList<ZonaComerciala<String,Integer>> listaZonaComerciala,Integer n,Integer s,Integer e,Integer v)
	{ 
		Integer nord,sud,est,vest;
		int pozitieZona=Integer.MAX_VALUE,pozitieMagazin=Integer.MAX_VALUE;
		Integer distanta=0;
		Integer distanta_min=Integer.MAX_VALUE;
		for(int i=0;i<listaZonaComerciala.size();i++)
		{
			for(int j=0;j<listaZonaComerciala.get(i).getMagazine().size();j++)
			{
				
				nord=listaZonaComerciala.get(i).getMagazine().get(j).gerCordNord();
				sud=listaZonaComerciala.get(i).getMagazine().get(j).gerCordSud();
				est=listaZonaComerciala.get(i).getMagazine().get(j).gerCordEst();
				vest=listaZonaComerciala.get(i).getMagazine().get(j).gerCordVest();
				distanta=Math.abs(((n+s)/2)-((nord+sud)/2)) + Math.abs(((e+v)/2)-((est+vest)/2));
				if(distanta_min>distanta)
				{
					distanta_min=distanta;
					pozitieZona=i;
					pozitieMagazin=j;
				}
			}
		}
		System.out.println("cea mai apropiata zona:"+listaZonaComerciala.get(pozitieZona).getDenumire());
		System.out.println(listaZonaComerciala.get(pozitieZona).getMagazine().get(pozitieMagazin));
		
	}
	public static void Ex2(ArrayList<ZonaParcari<String,Integer>> listaZonaParcari)
	{ 
		int pozitieZona=Integer.MAX_VALUE,pozitieParcare=Integer.MAX_VALUE;
		Integer numar=0;
		Integer nr_max=-1;
		for(int i=0;i<listaZonaParcari.size();i++)
		{
			for(int j=0;j<listaZonaParcari.get(i).getParcari().size();j++)
			{
				if(listaZonaParcari.get(i).getParcari().get(j).getLibere()!=0)
				{ 
					numar=listaZonaParcari.get(i).getParcari().get(j).getLibere();
				if(nr_max<numar)
				{
					nr_max=numar;
					pozitieZona=i;
					pozitieParcare=j;
				}}
			}
		}
		System.out.println("cea mai apropiata zona:"+listaZonaParcari.get(pozitieZona).getDenumire());
		System.out.println(listaZonaParcari.get(pozitieZona).getParcari().get(pozitieParcare));
		
	}
	public static void main(String[] args) {
	
		try {
			CitireParcari("parcari.txt");
			CitireMagazine("magazine.txt");
			
		} catch (FileNotFoundException e)
		{
			System.err.println("Eroare la citire, file not found");
		}
		Scanner sc=new Scanner(System.in);
		Integer cordN,cordS,cordE,cordV;
		cordN=sc.nextInt();
		cordS=sc.nextInt();
		cordE=sc.nextInt();
		cordV=sc.nextInt();
		Ex1(listaZonaParcari,cordN,cordS,cordE,cordV);
		Ex4(listaZonaComerciala,cordN,cordS,cordE,cordV);
		Ex2(listaZonaParcari);
		}

	

}
