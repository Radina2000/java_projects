import java.util.ArrayList;

public class ZonaComerciala<S,T> {
  
	private ArrayList<Magazine<S,T>> magazine;
	private S denumire;
	private T nord;
	private T sud;
	private T est;
	private T vest;
	public ZonaComerciala(S denumire,ArrayList<Magazine<S,T>>magazine,T nord,T sud,T est,T vest)
	{
		this.denumire=denumire;
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
		this.magazine=magazine;
	}
	public void setMagazine(ArrayList<Magazine<S,T>> magazine)
	{
		this.magazine=magazine;
	}
	public ArrayList<Magazine<S,T>> getMagazine()
	{
		return magazine;
	}
	
	public void setDenumire(S denumire)
	{
		this.denumire=denumire;
	}
	public S getDenumire()
	{
		return this.denumire;
	}
	public void setCoord(T nord,T sud,T vest,T est)
	{
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
	}
	public T gerCordNord()
	{
		return this.nord;
	}
	public T gerCordSud()
	{
		return this.sud;
	}
	public T gerCordEst()
	{
		return this.est;
	}
	public T gerCordVest()
	{
		return this.vest;
	}
	public String toString()
	{
		return "Zona comerciala "+denumire+"\n"+"coordonate:\n"+"Nord: "+nord+"\n"+"Sud: "+sud+"\n"+"Est: "+est+"\n"+"Vest: "+vest+"\n"+" si are urmatoarele magazine:\n"+magazine.toString();
	}
	
}
