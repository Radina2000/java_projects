import java.util.*; 


public class Parcari<S,T> {

	private S denumire;
	private T nrLocuri;
	private T libere;
	private T nord;
	private T sud;
	private T est;
	private T vest;
	public void setLibere(T libere)
	{
		this.libere=libere;
	}
	public T getLibere()
	{
		return this.libere;
	}
	public Parcari(S denumire,T nrLocuri,T nord,T sud,T est,T vest,T libere)
	{
		this.denumire=denumire;
		this.nrLocuri=nrLocuri;
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
		this.libere=libere;
	}
	public void setDenumire(S denumire)
	{
		this.denumire=denumire;
	}
	public S getDenumire()
	{
		return this.denumire;
	}
	public void setNrLocuri(T nrLocuri)
	{
		this.nrLocuri=nrLocuri;
	}
	public T getLocuri()
	{
		return this.nrLocuri;
	}
	public void setCoord(T nord,T sud,T vest,T est)
	{
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
	}
	public T gerCordNord()
	{
		return this.nord;
	}
	public T gerCordSud()
	{
		return this.sud;
	}
	public T gerCordEst()
	{
		return this.est;
	}
	public T gerCordVest()
	{
		return this.vest;
	}
	public String toString()
	{
		return "Parcarea "+denumire+" are "+nrLocuri+" locuri si are "+libere+" locuri libere\n"+"coordonate:\n"+"Nord: "+nord+"\n"+"Sud: "+sud+"\n"+"Est: "+est+"\n"+"Vest: "+vest+"\n";
	}
}
