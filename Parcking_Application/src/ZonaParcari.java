import java.util.ArrayList;

public class ZonaParcari<S,T> {
 
	private ArrayList<Parcari<S,T>> parcari;
	private S denumire;
	private T tarif;
	private T nord;
	private T sud;
	private T est;
	private T vest;
	public ZonaParcari(S denumire,ArrayList<Parcari<S,T>> parcari,T nord,T sud,T est,T vest)
	{
		this.denumire=denumire;
		//this.tarif=tarif;
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
		this.parcari=parcari;
	}
	public void setParcari(ArrayList<Parcari<S,T>> parcari)
	{
		this.parcari=parcari;
	}
	public ArrayList<Parcari<S,T>> getParcari()
	{
		return parcari;
	}	
	public void setDenumire(S denumire)
	{
		this.denumire=denumire;
	}
	public S getDenumire()
	{
		return this.denumire;
	}
	public void setTarif(T tarif)
	{
		this.tarif=tarif;
	}
	public T getTarif()
	{
		return this.tarif;
	}
	public void setCoord(T nord,T sud,T vest,T est)
	{
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
	}
	public T gerCordNord()
	{
		return this.nord;
	}
	public T gerCordSud()
	{
		return this.sud;
	}
	public T gerCordEst()
	{
		return this.est;
	}
	public T gerCordVest()
	{
		return this.vest;
	}
	public String toString()
	{
		return "Zona de parcari "+denumire+" are tariful "+tarif+"\n"+"coordonate:\n"+"Nord: "+nord+"\n"+"Sud: "+sud+"\n"+"Est: "+est+"\n"+"Vest: "+vest+"\n"+" si are parcariile:\n"+parcari.toString();
	}
	
	
}
