
public class Magazine<S,T> {
 
	private S denumire;
	private S categorie;
	private T nord;
	private T sud;
	private T est;
	private T vest;
	public Magazine(S denumire,S categorie,T nord,T sud,T est,T vest)
	{
		this.denumire=denumire;
		this.categorie=categorie;
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
	}
	public void setDenumire(S denumire)
	{
		this.denumire=denumire;
	}
	public S getDenumire() {
		return this.denumire;
	}
	public void setCategorie(S categorie)
	{
		this.categorie=categorie;
	}
	public S getCategorie()
	{
		return this.categorie;
	}
	public void setCoord(T nord,T sud,T vest,T est)
	{
		this.nord=nord;
		this.sud=sud;
		this.est=est;
		this.vest=vest;
	}
	public T gerCordNord()
	{
		return this.nord;
	}
	public T gerCordSud()
	{
		return this.sud;
	}
	public T gerCordEst()
	{
		return this.est;
	}
	public T gerCordVest()
	{
		return this.vest;
	}
	public String toString()
	{
		return "magazinul "+denumire+" din categoria "+categorie+"\n"+"coordonate:\n"+"Nord: "+nord+"\n"+"Sud: "+sud+"\n"+"Est: "+est+"\n"+"Vest: "+vest+"\n";
	}
}
