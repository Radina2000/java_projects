
import javax.persistence.EntityManager;
import javax.persistence.Query;
import entities.Tren;

public class TrenDAL {
	public void adaugaTren(EntityManager entManager, Tren tren)
	{
		Tren t = entManager.find(Tren.class, tren.getId());
		if(t == null) {
			entManager.getTransaction().begin();
		    entManager.persist(tren);
		    entManager.getTransaction().commit();
		}
		
	}
	
	public int getTrenID(EntityManager entManager)
	{
		Query query = entManager.createQuery("Select MAX(t.id) from Tren t ");
		int result = (int) query.getSingleResult();
		return result;
	}
	
	

}
