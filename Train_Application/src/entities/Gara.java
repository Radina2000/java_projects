package entities;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name="gara")
public class Gara implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name="id")
	private int id;
	
	@Column(name="nume_statie")
	private String nume_statie;
	
	@Column(name="numar_linii")
	private int numarLinii;
	
	public Gara()
	{
		super();
	}
	
	public Gara(int id, String nume_statie, int numarLinii) {
		super();
		this.id = id;
		this.nume_statie = nume_statie;
		this.numarLinii = numarLinii;
	}
	public String getNume_statie() {
		return nume_statie;
	}

	public void setNume_statie(String nume_statie) {
		this.nume_statie = nume_statie;
	}
	public int getNumarLinii() {
		return numarLinii;
	}
	
	public void setNumarLinii(int numarLinii) {
		this.numarLinii = numarLinii;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String toString() {
		return "Gara " + id + "are " + numarLinii + " linii.";
		
	}

}
