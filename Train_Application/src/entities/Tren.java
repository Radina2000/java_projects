package entities;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name="tren")
public class Tren implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name="id")
	private int id;
	@Column(name="statie")
	private String statie;
	
	@Column(name="ora_ajungere")
	private String oraAjungere;
	
	@Column(name="ora_plecare")
	private String oraPlecare;
	@Column(name="idTren")
	private String idTren;
	
	public Tren()
	{
		super();
	}

	public String getIdTren() {
		return idTren;
	}

	public void setIdTren(String idTren) {
		this.idTren = idTren;
	}

	public Tren(int id, String statie, String oraAjungere, String oraPlecare, String idTren) {
		super();
		this.id = id;
		this.statie = statie;
		this.oraAjungere = oraAjungere;
		this.oraPlecare = oraPlecare;
		this.idTren = idTren;
	}



	public String getStatie() {
		return statie;
	}

	public void setStatie(String statie) {
		this.statie = statie;
	}

	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	
	public String getOraAjungere() {
		return oraAjungere;
	}
	
	public void setOraAjungere(String oraAjungere) {
		this.oraAjungere = oraAjungere;
	}
	
	public String getOraPlecare() {
		return oraPlecare;
	}
	
	public void setOraPlecare(String oraPlecare) {
		this.oraPlecare = oraPlecare;
	}
	
	
	public String toString(int id, String oraAjungere, String oraPlecare)
	{
		return "Trenul " + id + " ajunge la ora " + oraAjungere + " si pleaca la ora " + oraPlecare;
	}
	
	
	

}
