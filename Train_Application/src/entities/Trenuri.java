package entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="trenuri")
public class Trenuri {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name="id")
	private int id;
	@Column(name="numarTren")
	private int numarTren;
	
	public Trenuri()
	{
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumarTren() {
		return numarTren;
	}

	public void setNumarTren(int numarTren) {
		this.numarTren = numarTren;
	}

	public Trenuri(int id, int numarTren) {
		super();
		this.id = id;
		this.numarTren = numarTren;
	}
}
