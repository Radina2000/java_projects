import javax.persistence.EntityManager;
import javax.persistence.Query;

import entities.Trenuri;

public class TrenuriDAL {
	public void adaugaTrenuri(EntityManager entManager, Trenuri trenuri)
	{
		Trenuri t = entManager.find(Trenuri.class, trenuri.getId());
		if(t == null) {
			entManager.getTransaction().begin();
		    entManager.persist(trenuri);
		    entManager.getTransaction().commit();
		}
		
	}
	
	public int getTrenuriID(EntityManager entManager)
	{
		Query query = entManager.createQuery("Select MAX(t.id) from Trenuri t ");
		int result = (int) query.getSingleResult();
		return result;
	}
}
