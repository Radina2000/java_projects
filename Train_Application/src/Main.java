import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entities.Gara;
import entities.Tren;
import entities.Trenuri;

public class Main 
{

	public static Gara adaugaGara(EntityManager entManager, Scanner sc, GaraDAL garaDAL) 
	{
		int id = garaDAL.getGaraID(entManager);
		System.out.println("Inserati numele statiei:");
		String statie=sc.next();
	    System.out.println("Inserati numarul de linii:");
	    int lini = sc.nextInt();
	    Gara gr = new Gara(id + 1, statie,lini);
	    garaDAL.adaugaGara(entManager, gr);
	    return gr;
	}	
	public static Tren adaugaTren(EntityManager entManager, Scanner sc, TrenDAL trenDAL)
	{
		System.out.println("Cate insertii doriti sa faceti?");
	    int nr = sc.nextInt();
	    for(int i=0;i<nr;i++)
		{
	    	int id = trenDAL.getTrenID(entManager);
	    	System.out.println("Inserati statia:");
	    	String station = sc.next();
	    	System.out.println("Inserati ora ajungere:");
	    	String oraAjungere = sc.next();
	    	System.out.println("Inserati ora plecare:");
	    	String oraPlecare = sc.next();
	    	System.out.println("Inserati id-ul trenului:");
	    	String idTren = sc.next();
	    	Tren t=new Tren(id+1,station,oraAjungere,oraPlecare,idTren);
	    	trenDAL.adaugaTren(entManager, t);
		}
		//return t;
		return null;
	}
	/*public static Gara verificareGaraLibera(EntityManager entManager, GaraDAL garaDAL,int id1,String numeGara1)
	{
		Gara gara;
		int id = garaDAL.getGaraID(entManager);
		String numeGara=garaDAL.getNameGara(entManager);
		//if()
		return null;
	}*/
	public static Trenuri adaugaTrenuri(EntityManager entManager, Scanner sc, TrenuriDAL trenuriDAL) 
	{
		int id = trenuriDAL.getTrenuriID(entManager);
	    System.out.println("Inserati numarul trenului:");
	    int numar_tren = sc.nextInt();
	    Trenuri t = new Trenuri(id + 1, numar_tren);
	    trenuriDAL.adaugaTrenuri(entManager, t);
	    return t;
	}
		
	public static void main(String[] args) 
	{
		EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("trenuriDB");
	    EntityManager entManager = emFactory.createEntityManager();
	    Scanner sc = new Scanner(System.in);
	    TrenDAL trenDAL = new TrenDAL();
	 	GaraDAL garaDAL = new GaraDAL();
	 	TrenuriDAL trenuriDAL=new TrenuriDAL();
	   
	    
	    Tren tren= adaugaTren(entManager, sc, trenDAL);
	    //Trenuri t= adaugaTrenuri(entManager, sc, trenuriDAL);
	    //Gara gara = adaugaGara(entManager,sc,garaDAL);
	   // Tren_Gara legatura=addLegatura(entManager, sc, trenDAL, garaDAL);
	    sc.close();
	    entManager.close();
	    emFactory.close();
	}

}
