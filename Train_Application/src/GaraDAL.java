
import javax.persistence.EntityManager;
import javax.persistence.Query;
import entities.Gara;

public class GaraDAL {
	public void adaugaGara(EntityManager entManager, Gara gara) {
		Gara gr= entManager.find(Gara.class, gara.getId());
		if(gr == null)
		{
			entManager.getTransaction().begin();
		    entManager.persist(gara);
		    entManager.getTransaction().commit();
		}
	}
	
	public int getGaraID(EntityManager entManager) {
		Query query = entManager.createQuery("Select MAX(gr.id) from Gara gr");
		int maxi = (int) query.getSingleResult();
		return maxi;
	}
	public String getNameGara(EntityManager entManager) {
		Query query = entManager.createQuery("Select gara.nume from Gara gr");
		String maxi = (String) query.getSingleResult();
		return maxi;
	}


}
