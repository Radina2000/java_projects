import javax.swing.*;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Vector;


public class Fereastra extends JPanel
{
    Vector<Arc>listaArce;
    Vector<Nod>listaNoduri;
    Point pointStart=null;
    Point pointEnd=null;
    Nod nodStart;
    Nod nodEnd;
    int numarNoduri;
    int numarMuchii;

    public Fereastra() throws FileNotFoundException, XMLStreamException
    {
        Vector<Nod>Noduri=new Vector<>();
        Vector<Arc>Arce=new Vector<>();

        setBackground(Color.cyan);
        final String fileLocation = "map2.xml";
        final String NODES_ELEMENT = "nodes";
        final String NODE_ELEMENT = "node";
        final String ARCS_ELEMENT = "arches";
        final String ARC_ELEMENT = "arch";

        FileInputStream fileInputStream = new FileInputStream(fileLocation);
        XMLStreamReader xmlStreamReader = XMLInputFactory.newInstance().createXMLStreamReader(fileInputStream);

        while (xmlStreamReader.hasNext())
        {
            int eventCode = xmlStreamReader.next();
            if ((XMLStreamConstants.START_ELEMENT == eventCode) 
            		&& xmlStreamReader.getLocalName().equalsIgnoreCase(NODES_ELEMENT))
            {

                while (xmlStreamReader.hasNext())
                {
                    eventCode = xmlStreamReader.next();
                    if ((XMLStreamConstants.END_ELEMENT == eventCode)
                            && xmlStreamReader.getLocalName().equalsIgnoreCase(NODES_ELEMENT)) 
                    {
                        break;
                    }
                    else
                    {
                        if ((XMLStreamConstants.START_ELEMENT == eventCode)
                                && xmlStreamReader.getLocalName().equalsIgnoreCase(NODE_ELEMENT)) 
                        {
                            int x=0, y=0, id=0;
                            int attributesCount = xmlStreamReader.getAttributeCount();
                            for (int i = 0; i < attributesCount; i++) {
                                if (i == 0)
                                    id = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                                if(i==1)
                                    x = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                                if(i==2)
                                    y = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                            }
                            System.out.println(id);
                            Nod nod = new Nod((int) ((x - 4945029) * 0.01861), (int) ((y - 573929) * 0.00894), id);
                            Noduri.add(nod);
                        }
                    }
                }
            }
            if ((XMLStreamConstants.START_ELEMENT == eventCode)
                    && xmlStreamReader.getLocalName().equalsIgnoreCase(ARCS_ELEMENT)) 
            {
                while (xmlStreamReader.hasNext())
                {
                    eventCode = xmlStreamReader.next();
                    if ((XMLStreamConstants.END_ELEMENT == eventCode)
                            && xmlStreamReader.getLocalName().equalsIgnoreCase(ARCS_ELEMENT))
                    {
                        break;
                    }
                    else 
                    {
                        if ((XMLStreamConstants.START_ELEMENT == eventCode)
                                && xmlStreamReader.getLocalName().equalsIgnoreCase(ARC_ELEMENT))
                        {
                            int id1 = 0, id2 = 0, lenght = 0;
                            int attributesCount = xmlStreamReader.getAttributeCount();
                            for (int i = 0; i < attributesCount; i++)
                            {
                                if (i == 0)
                                    id1 = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                                if (i == 1)
                                    id2 = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                                if (i == 2)
                                    lenght = Integer.parseInt(xmlStreamReader.getAttributeValue(i));
                            }

                            Arc arc=new Arc(Noduri.elementAt(id1).getCoordX(), Noduri.elementAt(id1).getCoordY(),
                                    Noduri.elementAt(id2).getCoordX(), Noduri.elementAt(id2).getCoordY(), lenght);
                            Arce.add(arc);
                        }
                    }
                }
            }
        }
        listaNoduri=Noduri;
        listaArce=Arce;
        repaint();
    }
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        for (Arc a : listaArce)
        {
            a.drawArc(g);
        }
        for(int i=0; i<listaNoduri.size(); i++)
        {
            int node_diam = 2;
            listaNoduri.elementAt(i).drawNode(g, node_diam);
        }
    }
}


