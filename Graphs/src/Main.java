
import javax.swing.*;
import javax.xml.stream.XMLStreamException;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public Main() {
    }

    private static void initUI() {
        JFrame f = new JFrame("Luxemburg");
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            f.add(new Fereastra());
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        f.setSize(1400, 800);
        f.setVisible(true);
    }

    public static void main(String[] args) 
    {
         System.out.println("Algoritmi:");
         System.out.println("1.BellmanFord");
         System.out.println("2.Dijkstra");
         System.out.println("Ce algoritm doriti sa aplicati?");
         Scanner citire=new Scanner(System.in);
         String cautare=citire.next();
         if(cautare.equals("1"))
         {
      	   int[][] matriceCost = new int[][] 
      		 {
                 { -1, 3, 1, -1, 8 },
                 { -1, -1, -1, 4, 6 },
                 { -1, 2, -1, -1, 1 },
                 { -1, -1, 3, -1, -1 },
                 { -1, -1, -1, 4, -1 },
      		 };
         BF bellmanFord = new BF();
         bellmanFord.algoritmBellmanFord(1, 5, matriceCost);
         System.out.println();
         }
         else
      	   if(cautare.equals("2"))
      	   {
      		   int[][] matriceCost = new int[][]
      		  {
                     { -1, 28, 1, 2, -1, -1, -1, -1 },
                     { -1, -1, -1, -1, 9, -1, 10, -1 },
                     { -1, 8, -1, -1, -1, -1, 26, -1 },
                     { -1, -1, -1, -1, -1, -1, 24, 27 },
                     { -1, -1, 5, -1, -1, 8, -1, 7 },
                     { -1, -1, -1, -1, -1, -1, -1, 7 },
                     { -1, -1, -1, -1, -1, 8, -1, 1 },
                     { -1, -1, -1, -1, -1, -1, -1, -1 }
             };
             Dijkstra dijkstra = new Dijkstra();
             dijkstra.algDijkstra(1, 8, matriceCost);
             System.out.println();
      	   }
      
    	System.out.println("Pentru afisarea hartii Luxemburgului apasati tasta 1");
    	 Scanner tasta=new Scanner(System.in);
         String c=tasta.next();
         if(c.equals("1"))
        {
        	 SwingUtilities.invokeLater(new Runnable()
        	 {
        		 public void run() 
        		 {
        			 Main.initUI();
        		 }
        	 });
        }
         
      
    }
}
