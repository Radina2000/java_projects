import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Arc
{
    private int startX;
    private int startY;
    private int endX;
    private int endY;
    private int lenght;

    public Arc(int startX,int startY, int endX,int endY, int lenght)
    {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.lenght=lenght;
    }

    public void drawArc(Graphics g)
    {
        g.setColor(Color.BLACK);
        g.drawLine(startX, startY, endX, endY);
    }
}
