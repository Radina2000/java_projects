import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Nod
{
    private int coordonataX;
    private int coordonataY;
    private int id;

    public Nod(int coordX, int coordY, int number)
    {
        this.coordonataX = coordX;
        this.coordonataY = coordY;
        this.id = number;
    }
    public int getCoordX() {
        return coordonataX;
    }
    public int getCoordY() {
        return coordonataY;
    }
    public int getId() {
        return id;
    }
    public void drawNode(Graphics g, int node_diam)
    {
        g.setColor(Color.BLACK);
        g.setFont(new Font("TimesRoman", Font.BOLD, 1));
        g.fillOval(coordonataX, coordonataY, node_diam, node_diam);
        g.setColor(Color.black);
        g.drawOval(coordonataX, coordonataY, node_diam, node_diam);
    }
}
