
import java.util.ArrayList;
import java.util.List;

public class Dijkstra 
{
    private List<Integer> d;
    private List<Integer> p;
    private List<Integer> w;

    public Dijkstra()
    {
        d = new ArrayList<>();
        p = new ArrayList<>();
        w = new ArrayList<>();
    }

    public void algDijkstra(int start, int n, int[][] matriceCost) 
    {
        for (int i = 0; i < n; i++) 
        {
            w.add(i + 1);
        }
        for (int i = 0; i < n; i++) 
        {
            p.add(0);
            d.add(-1);
        }
        d.set(start - 1, 0);
        while (!w.isEmpty()) 
        {
            Integer x = 1;
            Integer valX = -1;
            for (int i = 0; i < n; i++) 
            {
                if (w.contains(i + 1)) 
                {
                    if ((d.get(i) < valX && d.get(i) != -1) || (valX == -1 && d.get(i) > -1))
                    {
                        x = i + 1;
                        valX = d.get(i);
                    }
                }
            }
            w.remove(x);
            for (int i = 0; i < n; i++) 
            {
                Integer y = i + 1;
                if (w.contains(y) && matriceCost[x - 1][y - 1] > -1)
                {
                    if ((d.get(x - 1) + matriceCost[x - 1][y - 1] < d.get(y - 1)) || (d.get(y - 1) == - 1)) 
                    {
                        d.set(y - 1, d.get(x - 1) + matriceCost[x - 1][y - 1]);
                        p.set(y - 1, x);
                    }
                }
            }
        }
        System.out.print("Vectorul de parinti: ");
        for (int i = 0; i < p.size(); i++)
        {
            System.out.print(p.get(i) + " ");
        }
        System.out.println();
        System.out.print("Vectorul de costuri: ");
        for (int i = 0; i < d.size(); i++)
        {
            System.out.print(d.get(i) + " ");
        }
        System.out.println();
        System.out.print("Drumurile:");
        System.out.println();
        for (int i = 0; i < n; i++)
        {
            int nod = i + 1;
            if (p.get(i) != 0)
            {
                System.out.print("De la " + start + " la " + nod + ": ");
                System.out.print(start + " ");
                drum(start, i);
                System.out.println();
            }
        }
    }
    private void drum(int start, int i) {
        if (i != start - 1) {
            drum(start, p.get(i) - 1);
            int nod = i + 1;
            System.out.print(nod + " ");
        }
    }
}
