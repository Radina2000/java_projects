import java.util.ArrayList;
import java.util.List;

public class BF
{
	 private List<Integer> d;
	    private List<Integer> d1;
	    private List<Integer> p;

	    public BF()
	    {
	        d = new ArrayList<>();
	        d1 = new ArrayList<>();
	        p = new ArrayList<>();
	    }

	    public void algoritmBellmanFord(int start, int n, int[][] matriceCost)
	    {
	        for (int i = 0; i < n; i++)
	        {
	            d.add(-1);
	            p.add(0);
	        }
	        d.set(start - 1, 0);
	        for (int k = 0; k < n; k++) 
	        {
	            //int val = -1;
	            for (int i = 0; i < n; i++) 
	            {
	                for (int j = 0; j < n; j++) 
	                {
	                    if (matriceCost[i][j] > -1)
	                    {
	                        if (d.get(i) + matriceCost[i][j] < d.get(j) || d.get(j) == -1) 
	                        {
	                            d.set(j, d.get(i) + matriceCost[i][j]);
	                            p.set(j, i + 1);
	                        }
	                    }
	                }
	            }
	        }
	        System.out.print("Vectorul de parinti: ");
	        for (int i = 0; i < p.size(); i++)
	        {
	            System.out.print(p.get(i) + " ");
	        }
	        System.out.println();
	        System.out.print("Vectorul de costuri: ");
	        for (int i = 0; i < d.size(); i++)
	        {
	            System.out.print(d.get(i) + " ");
	        }
	        System.out.println();
	        System.out.print("Drumurile:");
	        System.out.println();
	        for (int i = 0; i < n; i++)
	        {
	            int nod = i + 1;
	            if (p.get(i) != 0) 
	            {
	                System.out.print("De la " + start + " la " + nod + ": ");
	                System.out.print(start + " ");
	                drum(start, i);
	                System.out.println();
	            }
	        }
	    }

	    private void drum(int start, int i) 
	    {
	        if (i != start - 1) {
	            drum(start, p.get(i) - 1);
	            int nod = i + 1;
	            System.out.print(nod + " ");
	        }
	    }
}